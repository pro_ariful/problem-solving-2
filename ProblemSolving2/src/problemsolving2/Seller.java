/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package problemsolving2;
import java.util.Vector;

/**
 *
 * @author Ariful, Hekam, Mirza, Evan, John
 *
 */

public class Seller extends User implements ViewTotalNo {
    private int itemsNum; //number of products seller added
    Vector<Item> myItems;

    Seller(){

    }
    public Seller(int userId,String name) {
        super(userId,name);

        super.accountType="Seller";
        myItems = new Vector<Item>();
        itemsNum=myItems.size();
    }

    public int getProductsNum() {
        return itemsNum;
    }

    @Override
    public void addItem(Item i) {
        myItems.add(i);
    }

    public void  removeItem(Item i){
        assert myItems.contains(i) : "The item doesn't exist in your list";
        myItems.remove(i);
    }

    public void updateQuantity(Item i, int q){

        i.setQuantity(q);
    }

    @Override
    public void printInfo() {
        System.out.println("    Seller Information");
        System.out.println("********************************");
        super.printInfo();
        getTotalItemsNo();
    }

    public void getTotalItemsNo() {
        assert  myItems != null : "There is no items list";
        itemsNum=myItems.size();
        System.out.println("Number of items: " + itemsNum);

    }
}
