/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package problemsolving2;

/**
 *
 * @author Ariful, Hekam, Mirza, Evan, John
 */
public class ShoppingApplication {
    public static void main(String[] args) {

        //Products
        Item watch = new Item(231,"Watch",54.99);
        Item bags = new Item(682,"Bags",119.99);
        Item laptop = new Item(102,"Laptop",84.90);
        Item monitor = new Item(422,"Monitor",15.45);
        Item mouse = new Item(422,"Mouse",19.80);
        Item console = new Item(422,"Game Console",9.99);

        //Sellers
        Seller shopA = new Seller(1662,"H&M");
        Seller shopB = new Seller(1662,"Watsons");
        //Shop A products
        shopA.addItem(watch);
        shopA.updateQuantity(watch,2);

        shopA.addItem(watch);
        shopA.updateQuantity(bags,2);

        shopA.addItem(laptop);
        shopA.updateQuantity(laptop,3);

        //Shop B products
        shopB.addItem(monitor);
        shopB.updateQuantity(monitor,10);

        shopB.addItem(mouse);
        shopB.updateQuantity(mouse,0);

        shopB.addItem(console);
        shopB.updateQuantity(console,7);

        //Addresses
        Address ad1 = new Address("Taman Danga Bay", "Johor", "Malaysia");
        Address ad2 = new Address("LOT 1-9", "Selengor", "Kuala Lumpur");

        // Customers:
        Buyer boy=new Buyer(111,"Ariful",140);
        boy.setAddress(ad1);

        Buyer girl=new Buyer(121,"Neha",290);
        girl.setAddress(ad2);

        //Adding Products
        boy.setSeller(shopA);
        girl.setSeller(shopB);

        boy.addItem(watch);
        boy.addItem(bags);
        boy.addItem(laptop);
        boy.addItem(monitor);

        girl.addItem(monitor);
        girl.addItem(monitor);
        girl.addItem(mouse);
        girl.addItem(mouse);
        girl.addItem(mouse);
        girl.addItem(console);

        girl.removeItem(monitor);

        // Order
        Order boyOrder = new Order(1234561);
        
        boyOrder.setSeller(boy.getSeller());
        boyOrder.setCart(boy.getCart());

        Order girlOrder = new Order(1234562);
        
        girlOrder.setSeller(girl.getSeller());
        girlOrder.setCart(girl.getCart());

        //Printing Order
        boyOrder.printOrder();
        girlOrder.printOrder();
    }
}


