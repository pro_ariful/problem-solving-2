/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package problemsolving2;

/**
 *
 * @author Ariful, Hekam,Mirza, Evan, John
 */
import java.util.Vector;

public class Buyer extends User {
    private Seller S;
    private Address address;
    private double accountBalance;
    private Vector<Item> cart;


    public Buyer(){
        // INVARIANT
        assert this.accountBalance >= 0 : "Account Balance Can not be zero";
    }


    public Buyer(int userId, String name, double accountBalance){

        super(userId,name);

        // INVARIANT
        assert this.accountBalance > 0 : "Account Balance Can not be negative";

        this.accountBalance=accountBalance;
        super.accountType="Customer";
        address=new Address();
        Seller S=new Seller();
        cart= new Vector<Item>();
    }

    public double getAccountBalance() {
        return accountBalance;
    }

    public Vector<Item> getCart() {
        return cart;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public void setSeller(Seller s) {
        S = s;
    }

    public Seller getSeller() {
        return S;
    }

    public void addItem(Item i) {
        assert accountBalance>i.getItemCost() : "no balance";
        if(accountBalance<i.getItemCost() && S.myItems.contains(i)){
            System.out.println("You (" + name + ") Have insufficient balance to add item: "+i.getItemName());
        }

        if (i.getQuantity()== 0){
            System.out.println("Item: "+i.getItemName() + " is sold out.");
        }

        if (!S.myItems.contains(i)){
            System.out.println("Item: "+i.getItemName()+" is not Sold by " +
                    S.getName());
        }
        if((accountBalance>=i.getItemCost()) && (i.getQuantity()>0) && S.myItems.contains(i)){
            accountBalance-=i.getItemCost();
            i.setQuantity(i.getQuantity()-1);
            cart.add(i);
        }
    }

    public void removeItem(Item i) {
//        assert cart.contains(p);
        if(cart.contains(i)){
            cart.remove(i);
            accountBalance+=i.getItemCost();
            i.setQuantity(i.getQuantity()+1);
        }
        else{
            System.out.println("Item: "+ i.getItemName() + "was not added to cart");
        }

    }

    @Override
    public void printInfo() {
        System.out.println("    Customer Information");
        System.out.println("********************************");
        super.printInfo();
        System.out.println("Address: " + address.getFullAddress());

    }

   


}

