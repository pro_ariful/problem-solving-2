/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package problemsolving2;

/**
 *
 * @author Ariful, Hekam, Mirza, Evan, John
 */
public class Item {
    private int itemId;
    private String itemName;
    private int quantity;
    private double itemCost;

    public Item(int itemId,String itemName,double itemCost)
    {
        // INVARIANT
        assert this.quantity >= 0 :"Quantity can't be negative";
        assert this.itemCost >0 :"Product cost must be more than 0!";

        this.itemId=itemId;
        this.itemName=itemName;
        this.itemCost=itemCost;

    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getItemId(){return itemId;}
    public String getItemName(){return itemName;}
    public int getQuantity(){return quantity;}
    public double getItemCost(){return itemCost;}

    public void printInfo(){
        System.out.println("Item ID: " + itemId);
        System.out.println("Item Name: " + itemName);
        System.out.println("Price: " + itemCost);
    }
}
